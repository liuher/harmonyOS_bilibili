---
sidebar_position: 1
id: dev
sidebar_label: 网站规划
slug: /community/dev
---

# 网站规划

## 已有功能

- ✅ [网站首页](https://hm.codefe.cn/)
- ✅ [课程](https://hm.codefe.cn/docs/intro/)
- ✅ [博客](https://hm.codefe.cn/blog)
- ✅ [社区-团队](https://hm.codefe.cn/team)

## 开发中

- [ ] [案例展示](#)
- [ ] [留言板](#)

:::tip

由于个人精力有限，网站开发进度缓慢。真诚地邀请有志之士加入我们的团队，共同推动网站的开发。

欢迎有兴趣的朋友联系我们。

:::

## 未来规划

欢迎大家提出宝贵建议

- [ ] [xxx](#)
